#[derive(Clone, Copy)]
pub struct Color {
  r: u8,
  g: u8,
  b: u8,
}

impl Color {
  pub fn black() -> Color {
    Color {
      r: 0x00,
      g: 0x00,
      b: 0x00,
    }
  }

  pub fn white() -> Color {
    Color {
      r: 0xff,
      g: 0xff,
      b: 0xff,
    }
  }

  pub fn r(&self) -> u8 {
    self.r
  }

  pub fn g(&self) -> u8 {
    self.g
  }

  pub fn b(&self) -> u8 {
    self.b
  }
}
