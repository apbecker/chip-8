use sdl2::audio::{AudioQueue, AudioSpecDesired};

use crate::timer::Timer;

const SAMPLE_FREQ: i32 = 44_100;
const NUM_SAMPLES: i32 = SAMPLE_FREQ / 60;

#[derive(Debug)]
pub enum Error {
  AudioInit(String),
  AudioOpen(String),
}

pub struct Audio {
  device: AudioQueue<f32>,
}

impl Audio {
  pub fn new(sdl: &sdl2::Sdl) -> Result<Audio, Error> {
    let audio = sdl.audio().map_err(Error::AudioInit)?;

    let spec = AudioSpecDesired {
      channels: Some(1),
      freq: Some(SAMPLE_FREQ),
      samples: Some(NUM_SAMPLES as u16),
    };

    let device = audio.open_queue(None, &spec).map_err(Error::AudioOpen)?;
    device.resume();

    Ok(Audio { device })
  }

  pub fn render(&self, timer: &mut Timer) {
    let buffer = timer.wave(NUM_SAMPLES);
    let result = self.device.queue(&buffer);

    debug_assert!(result, "queue invocation returned false");
  }
}
