use sdl2::{event::Event, keyboard::Keycode, EventPump};

use crate::keyboard::{Key, KeyState, Keyboard};

// 1 2 3 C
// 4 5 6 D
// 7 8 9 E
// A 0 B F

#[derive(Debug)]
pub enum Error {
  InputInit(String)
}

pub struct Input(EventPump);

impl Input {
  pub fn new(sdl: &sdl2::Sdl) -> Result<Input, Error> {
    let event_pump = sdl.event_pump().map_err(Error::InputInit)?;

    Ok(Input(event_pump))
  }

  pub fn update(&mut self, keyboard: &mut Keyboard) -> bool {
    for event in self.0.poll_iter() {
      match event {
        Event::Quit { .. }
        | Event::KeyDown {
          keycode: Some(Keycode::Escape),
          ..
        } => return false,

        Event::KeyDown {
          keycode: Some(key), ..
        } => map_key(key, |&key| keyboard.set_key_state(key, KeyState::Pressed)),

        Event::KeyUp {
          keycode: Some(key), ..
        } => map_key(key, |&key| keyboard.set_key_state(key, KeyState::Released)),

        _ => continue,
      }
    }

    return true;
  }
}

fn map_key<F: FnOnce(&Key)>(key: Keycode, callback: F) {
  match key {
    Keycode::Num1 => (callback)(&Key::new(0x1)),
    Keycode::Num2 => (callback)(&Key::new(0x2)),
    Keycode::Num3 => (callback)(&Key::new(0x3)),
    Keycode::Num4 => (callback)(&Key::new(0xc)),
    Keycode::Q => (callback)(&Key::new(0x4)),
    Keycode::W => (callback)(&Key::new(0x5)),
    Keycode::E => (callback)(&Key::new(0x6)),
    Keycode::R => (callback)(&Key::new(0xd)),
    Keycode::A => (callback)(&Key::new(0x7)),
    Keycode::S => (callback)(&Key::new(0x8)),
    Keycode::D => (callback)(&Key::new(0x9)),
    Keycode::F => (callback)(&Key::new(0xe)),
    Keycode::Z => (callback)(&Key::new(0xa)),
    Keycode::X => (callback)(&Key::new(0x0)),
    Keycode::C => (callback)(&Key::new(0xb)),
    Keycode::V => (callback)(&Key::new(0xf)),
    _ => (),
  }
}
