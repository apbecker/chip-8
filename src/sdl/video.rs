use sdl2::{
  pixels::PixelFormatEnum,
  render::{Canvas, TextureValueError},
  video::{Window, WindowBuildError},
  IntegerOrSdlError,
};

use crate::{
  display::{Display, Pixel},
  sdl::color::Color,
};

#[derive(Debug)]
pub enum Error {
  VideoInit(String),
  CanvasBuild(IntegerOrSdlError),
  TextureBuild(TextureValueError),
  TextureCopy(String),
  TextureLock(String),
  WindowBuild(WindowBuildError),
}

pub struct Video(Canvas<Window>);

impl Video {
  pub fn new(sdl: &sdl2::Sdl) -> Result<Video, Error> {
    let video = sdl.video().map_err(Error::VideoInit)?;

    const WINDOW_SCALE: u32 = 8;

    let width = Display::width() as u32;
    let height = Display::height() as u32;

    let canvas = video
      .window("CHIP8", WINDOW_SCALE * width, WINDOW_SCALE * height)
      .build()
      .map_err(Error::WindowBuild)?
      .into_canvas()
      .accelerated()
      .present_vsync()
      .build()
      .map_err(Error::CanvasBuild)?;

    Ok(Video(canvas))
  }

  fn get_color(display: &Display, x: usize, y: usize) -> Color {
    match display.pixel(x, y) {
      Pixel::Off => Color::black(),
      Pixel::On => Color::white(),
    }
  }

  pub fn render(&mut self, display: &Display) -> Result<(), Error> {
    let texture_creator = self.0.texture_creator();
    let mut texture = texture_creator
      .create_texture_streaming(
        PixelFormatEnum::RGB888,
        Display::width() as u32,
        Display::height() as u32,
      )
      .map_err(Error::TextureBuild)?;

    texture
      .with_lock(None, |pixels, pitch| {
        for y in 0..Display::height() {
          for x in 0..Display::width() {
            let color = Self::get_color(display, x, y);
            pixels[(y * pitch) + (x * 3) + 0] = color.b();
            pixels[(y * pitch) + (x * 3) + 1] = color.g();
            pixels[(y * pitch) + (x * 3) + 2] = color.r();
          }
        }
      })
      .map_err(Error::TextureLock)?;

    self.0.copy(&texture, None, None).map_err(Error::TextureCopy)?;
    self.0.present();

    Ok(())
  }
}
