const FREQUENCY: f64 = 440.0;
const SAMPLE_FREQUENCY: f64 = 44_100.0;
const RATE: f64 = (std::f64::consts::TAU * FREQUENCY) / SAMPLE_FREQUENCY;

pub struct Timer {
  delay: i32,
  sound: i32,
  level: f64,
}

impl Timer {
  pub fn new() -> Timer {
    Timer {
      delay: 0,
      sound: 0,
      level: 0.0,
    }
  }

  pub fn delay(&self) -> i32 {
    self.delay
  }

  pub fn set_delay(&mut self, value: i32) {
    self.delay = value;
  }

  pub fn set_sound(&mut self, value: i32) {
    self.sound = value;
  }

  pub fn update(&mut self) {
    self.delay = std::cmp::max(0, self.delay - 1);
    self.sound = std::cmp::max(0, self.delay - 1);
  }

  pub fn wave(&mut self, num_samples: i32) -> Vec<f32> {
    let mut samples = vec![];

    for _ in 1..num_samples {
      samples.push((self.level.sin() / 2.0) as f32);

      if self.sound > 0 {
        self.level = (self.level + RATE) % std::f64::consts::TAU;
      }
    }

    samples
  }
}
