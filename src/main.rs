use cpu::Cpu;
use sdl::{audio::Audio, input::Input, video::Video};

mod cpu;
mod display;
mod keyboard;
mod memory;
mod random;
mod sdl;
mod stack;
mod timer;

fn main() {
  let argv: Vec<_> = std::env::args().collect();
  if argv.len() == 2 {
    let game = std::fs::read(&argv[1]).expect("unable to open game");
    run_game(&game).expect("unable to run game");
  } else {
    println!("Wrong number of parameters");
  }
}

#[derive(Debug)]
pub enum Error {
  Audio(sdl::audio::Error),
  Input(sdl::input::Error),
  Video(sdl::video::Error),
}

fn run_game(game: &[u8]) -> Result<(), Error> {
  let sdl = sdl2::init().expect("couldn't initialize sdl");

  let audio = Audio::new(&sdl).map_err(Error::Audio)?;
  let mut input = Input::new(&sdl).map_err(Error::Input)?;
  let mut video = Video::new(&sdl).map_err(Error::Video)?;

  let mut cpu = Cpu::new(game);

  loop {
    cpu.step_for(25);

    if !input.update(&mut cpu.keyboard) {
      break;
    }

    audio.render(&mut cpu.timer);
    video.render(&cpu.display).map_err(Error::Video)?;
  }

  Ok(())
}
